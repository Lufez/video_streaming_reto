﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Design;
using DA;


namespace Logic
{
    public class VideoLo
    {
        public SqlConnection conexion;
        public SqlTransaction transaccion;
        public string error;
        

        public VideoLo()
        {
            this.conexion = Conexion.getConexion();//Obtiene la conexion a la base de datos
            
        }
        /************Agregar Video*****************/
        public bool agregarVideo(VideoDis video, string Identificador)
        {
            //primero se consulta si esta la pelicula o serie en la base de datos
            
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandText = "select *from InfoVideos";
            SqlDataReader registro = comando.ExecuteReader();// Lee el dato consultado en registro
            VideoDis final = new VideoDis(); //variable vacia para guardar la informacion de la pelicula que tenga a coicidencia
            final.Titulo = "";
            final.Duracion = 0;
            final.Año = 0;
            final.Reseña = "";
            final.Genero = "";
            final.Tipo = "";
            while (registro.Read()) //se analiza la base de datos para encontrar la corresponidente igualdad
            {
                VideoDis Video = new VideoDis();
                Video.Titulo = registro.GetString(0);
                Video.Duracion = registro.GetInt32(1);
                Video.Año = registro.GetInt32(2);
                Video.Reseña = registro.GetString(3);
                Video.Genero = registro.GetString(4);
                Video.Tipo = registro.GetString(5);
                if (Video.Tipo == "Serie") //si es una serie se ponen los datos de capitulos y temporadas, si es pelicula no se ponen
                {
                    Video.Capitulos = registro.GetInt32(6);
                    Video.Temporadas = registro.GetInt32(7);
                }

                if (Identificador == Video.Titulo)
                {
                    final.Titulo = Video.Titulo;
                }
            }
            if (final.Titulo == Identificador)
            {
                registro.Close();
                return false;
            }
            else
            {
                //Acceso a base de datos
                registro.Close(); //primero se cierra el registro 
                bool agregar = false;
                comando.Connection = conexion; //comando de conexion
                comando.CommandText = "insert into InfoVideos values(@titulo, @duracion, @año, @reseña, @genero, @tipo, @capitulos, @temporadas)";

                /*************Preparando para inserta los datos del video en la tabla de la base de datos*************/

                comando.Parameters.AddWithValue("@titulo", video.Titulo);
                comando.Parameters.AddWithValue("@duracion", video.Duracion);
                comando.Parameters.AddWithValue("@año", video.Año);
                comando.Parameters.AddWithValue("@reseña", video.Reseña);
                comando.Parameters.AddWithValue("@genero", video.Genero);
                comando.Parameters.AddWithValue("@tipo", video.Tipo);
                comando.Parameters.AddWithValue("@capitulos", video.Capitulos);
                comando.Parameters.AddWithValue("@temporadas", video.Temporadas);
                try
                {
                    comando.ExecuteNonQuery(); //Realizar la operacion de insertar en la base de datos
                    agregar = true;
                }
                catch (SqlException ex)
                {
                    this.error = ex.Message;
                }
                
                return agregar; //Se inserto correctamente
            }
            

        }
        /*******************Consultar Video********************/
        public VideoDis ConsultarVideo(string Identificador)
        {
         
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandText = "select *from InfoVideos";
            SqlDataReader registro = comando.ExecuteReader();// Lee el dato consultado en registro
            VideoDis final = new VideoDis(); //variable vacia para guardar la informacion de la pelicula que tenga a coicidencia
            final.Titulo = "";
            final.Duracion = 0;
            final.Año = 0;
            final.Reseña = "";
            final.Genero = "";
            final.Tipo = "";
            while (registro.Read()) //se analiza la base de datos para encontrar la corresponidente igualdad
            {
                VideoDis video = new VideoDis();
                video.Titulo = registro.GetString(0);
                video.Duracion = registro.GetInt32(1);
                video.Año = registro.GetInt32(2);
                video.Reseña = registro.GetString(3);
                video.Genero = registro.GetString(4);
                video.Tipo = registro.GetString(5);
                //si es una serie se ponen los datos de capitulos y temporadas, si es pelicula no se ponen
                if (video.Tipo == "Serie") 
                {
                    video.Capitulos = registro.GetInt32(6);
                    video.Temporadas = registro.GetInt32(7);
                }
                //de acuerdo al identificador, se guarda el video consultado en la variable final
                if (Identificador == video.Titulo) 
                {
                    final.Titulo = video.Titulo;
                    final.Duracion = video.Duracion;
                    final.Año = video.Año;
                    final.Reseña = video.Reseña;
                    final.Genero = video.Genero;
                    final.Tipo = video.Tipo;
                    if (video.Tipo == "Serie")
                    {
                        final.Capitulos = video.Capitulos;
                        final.Temporadas = video.Temporadas;
                    }
                }
            }
            if (final.Titulo == Identificador)
            {
                registro.Close();
                return final;
            }
            else
            {
                registro.Close();
                return null;
            }
            
            

        }
        /***************se visualiza los datos de todos los videos***********/
        public List<VideoDis> listarvideos()
        {
            
            List<VideoDis> listaVideos = new List<VideoDis>(); //se crea lista para guardar
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandText = "select *from InfoVideos";
            SqlDataReader registro = comando.ExecuteReader(); //Leer Datos de la base de Datos
            while (registro.Read())
            {
                VideoDis video = new VideoDis();
                video.Titulo = registro.GetString(0);
                video.Duracion = registro.GetInt32(1);
                video.Año = registro.GetInt32(2);
                video.Reseña = registro.GetString(3);
                video.Genero = registro.GetString(4);
                video.Tipo = registro.GetString(5);
                if (video.Tipo == "Serie")
                {
                    video.Capitulos = registro.GetInt32(6);
                    video.Temporadas = registro.GetInt32(7); 
                }

                listaVideos.Add(video); // se agrega a la lista cada persona que encuentra
            }
            registro.Close();
            return listaVideos;
            
        }

        public int ModificarVideo(VideoDis video)
        {
            
            if (ConsultarVideo(video.Titulo) == null)
            {
                return 0;
            }
            else
            {
                //Asignar objetos
                SqlCommand comando = new SqlCommand();
                SqlDataAdapter Adactor = new SqlDataAdapter();
                DataSet dsVideo = new DataSet();
                Adactor.SelectCommand = comando;
                comando.Connection = conexion;
                //Comando para acceder al la tabla que se desea del sql server
                comando.CommandText = "select *from InfoVideos";
                //Se llena ds con los datos de esta tabla
                Adactor.FillSchema(dsVideo, SchemaType.Source, "InfoVideos");
                Adactor.Fill(dsVideo, "InfoVideos");
                SqlCommandBuilder objComandos = new SqlCommandBuilder(Adactor);
                //Se crea una tabla para trabajar con los datos del data set
                DataTable tbUsuario;
                //Se pone la informacion de a base de datos en la tabla
                tbUsuario = dsVideo.Tables["InfoVideos"];
                //Se declaa una variable, para hacer la busqueda con la respectiva llave primaria
                DataRow Buscar;
                //se busca el video en cuestion
                Buscar = tbUsuario.Rows.Find(video.Titulo);
                Buscar.BeginEdit();
                Buscar["Duracion en Minutos"] = video.Duracion;
                Buscar["Año de Estreno"] = video.Año;
                Buscar["Reseña"] = video.Reseña;
                Buscar["Genero"] = video.Genero;
                Buscar["Tipo (Serie o Pelicula)"] = video.Tipo;
                if (video.Tipo == "Serie")
                {
                    Buscar["Cantidad de Capitulos"] = video.Capitulos;
                    Buscar["Cantidad de Temporadas"] = video.Temporadas;
                }
                else
                {
                    Buscar["Cantidad de Capitulos"] = 0;
                    Buscar["Cantidad de Temporadas"] = 0;
                }
                Buscar.EndEdit();
                Adactor.Update(dsVideo, "InfoVideos");
                return 1;
            }

        }

        public int EliminarVideo(VideoDis video)
        {

            if (ConsultarVideo(video.Titulo) == null)
            {
                return 0;
            }
            else
            {
                //Asignar objetos
                SqlCommand comando = new SqlCommand();
                SqlDataAdapter Adactor = new SqlDataAdapter();
                DataSet dsVideo = new DataSet();
                Adactor.SelectCommand = comando;
                comando.Connection = conexion;
                //Comando sql a ejecutar, accedemos a la base de datos
                comando.CommandText = "select *from InfoVideos";
                //Llenar Usuario con los datos de la tabla InfoVideos
                Adactor.FillSchema(dsVideo, SchemaType.Source, "InfoVideos");
                Adactor.Fill(dsVideo, "InfoVideos");
                SqlCommandBuilder objComandos = new SqlCommandBuilder(Adactor);
                DataTable tbUsuario;
                tbUsuario = dsVideo.Tables["InfoVideos"];
                DataRow Buscar;
                //Se busca el video a eliminar
                Buscar = tbUsuario.Rows.Find(video.Titulo);
                //Se elimina video de la tabla de datos
                Buscar.Delete();
                // se suben los cambios a la base de datos, con el video eliminado
                Adactor.Update(dsVideo, "InfoVideos");
                return 1;
            }

        }



    }



}
