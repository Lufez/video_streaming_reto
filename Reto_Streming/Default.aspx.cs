﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logic;
using Design;


namespace Reto_Streming
{
    public partial class _Default : Page
    {
        VideoLo ObjvideoDis = new VideoLo(); // Para modificar los datos
        protected void Page_Load(object sender, EventArgs e)
        {
            Button1.Visible = false;

        }
        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            if (grillaVideos.Visible == true)
            {
                Button1.Visible = true;
            }
            VideoDis video = new VideoDis();
            string identificador = txtTitulo.Text;
            video.Titulo = txtTitulo.Text;
            video.Duracion = Convert.ToInt32(txtDuracion.Text);
            video.Año = Convert.ToInt32(txtAño.Text);
            video.Reseña = txtReseña.Text;
            video.Genero = txtGenero.Text;
            video.Tipo = cbTipo.Text;
            if (video.Tipo == "Serie")
            {
                video.Capitulos = Convert.ToInt32(txtCapitulos.Text);
            video.Temporadas = Convert.ToInt32(txtTemporadas.Text);
            }
            
            bool agregado = ObjvideoDis.agregarVideo(video,identificador);

            if (agregado)
            {
                lblMensaje.Text = "Video Agregado a la Base de Datos";
                limpiar();
            }
            else
            {
                lblMensaje.Text = "No se puede agregar un video ya existente";
            }

        }

        private void limpiar()
        {
            txtTitulo.Text = "";
            txtDuracion.Text = "";
            txtAño.Text = "";
            txtReseña.Text = "";
            txtCapitulos.Text = "";
            txtTemporadas.Text = "";
        }

        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            if (grillaVideos.Visible == true)
            {
                Button1.Visible = true;
            }
            string identificador = txtTitulo.Text;
            VideoDis video = ObjvideoDis.ConsultarVideo(identificador);
            if (video != null)
            {
                txtTitulo.Text = video.Titulo;
                txtDuracion.Text = Convert.ToString(video.Duracion);
                txtAño.Text = Convert.ToString(video.Año);
                txtReseña.Text = video.Reseña;
                txtGenero.Text = video.Genero;
                cbTipo.SelectedValue = video.Tipo;
                if (cbTipo.SelectedValue == "Serie") //Mostrar capitulos o temporadas dependiendo de si es una serie
                {
                    txtCapitulos.Text = Convert.ToString(video.Capitulos);
                    txtTemporadas.Text = Convert.ToString(video.Temporadas);
                    txtCapitulos.Visible = true;
                    Label7.Visible = true;
                    txtTemporadas.Visible = true;
                    Label8.Visible = true;
                }
                else
                {
                    txtCapitulos.Visible = false;
                    Label7.Visible = false;
                    txtTemporadas.Visible = false;
                    Label8.Visible = false;
                }
                lblMensaje.Text = "Video Encontrado";
            }
            else
            {
                lblMensaje.Text = "No existe este video";
            }
        }

        protected void btnListar_Click(object sender, EventArgs e)
        {
            List<VideoDis> ListaVideos = ObjvideoDis.listarvideos();
            if (ListaVideos.Count == 0)
            {
                lblMensaje.Text = "No hay Personas Agregadas en la base de datos";
            }
            else
            {
                grillaVideos.Visible = true;
                grillaVideos.DataSource = ListaVideos;
                grillaVideos.DataBind();
                Button1.Visible = true;
                lblMensaje.Text = "Lista de la base de datos";

            }
        }

        protected void cbTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Mostrar capitulos o temporadas dependiendo de si es una serie o una pelicula
            txtCapitulos.Visible = cbTipo.SelectedValue == "Serie";
            Label7.Visible = txtCapitulos.Visible;
            txtTemporadas.Visible = txtCapitulos.Visible;
            Label8.Visible = txtCapitulos.Visible;

        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtTitulo.Text = "";
            lblMensaje.Text = "";
            txtDuracion.Text = "";
            txtAño.Text = "";
            txtReseña.Text = "";
            txtCapitulos.Text = "";
            txtTemporadas.Text = "";
            grillaVideos.Visible = false;
            Button1.Visible = false;

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            grillaVideos.Visible = false;
            lblMensaje.Text = "";
            Button1.Visible = false;
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            if (grillaVideos.Visible == true)
            {
                Button1.Visible = true;
            }
            if (txtTitulo.Text == "" || txtDuracion.Text == "" || txtAño.Text == "" || txtReseña.Text == "" || txtGenero.Text == "" || cbTipo.Text == "" || txtCapitulos.Text == "" || txtTemporadas.Text == "")
            {

                if (cbTipo.Text == "Pelicula")
                {
                    if (txtTitulo.Text == "" || txtDuracion.Text == "" || txtAño.Text == "" || txtReseña.Text == "" || txtGenero.Text == "" || cbTipo.Text == "")
                    {
                        lblMensaje.Text = "Rellene todos los datos";
                    }
                    else
                    {
                        VideoDis video = new VideoDis();
                        int mod = 0;
                        video.Titulo = txtTitulo.Text;
                        video.Duracion = Convert.ToInt32(txtDuracion.Text);
                        video.Año = Convert.ToInt32(txtAño.Text);
                        video.Reseña = txtReseña.Text;
                        video.Genero = txtGenero.Text;
                        video.Tipo = cbTipo.Text;
                        if (video.Tipo == "Serie")
                        {
                            video.Capitulos = Convert.ToInt32(txtCapitulos.Text);
                            video.Temporadas = Convert.ToInt32(txtTemporadas.Text);
                        }
                        mod = ObjvideoDis.ModificarVideo(video);
                        if (mod == 1)
                        {
                            lblMensaje.Text = "Base de Datos Actualizada";
                            txtTitulo.Text = "";
                            txtDuracion.Text = "";
                            txtAño.Text = "";
                            txtReseña.Text = "";
                            txtCapitulos.Text = "";
                            txtTemporadas.Text = "";

                        }
                        else
                        {
                            lblMensaje.Text = "Video no Encontrado";
                        }

                    }
                }
                else
                {
                    lblMensaje.Text = "Rellene todos los datos";
                }
            }
            else
            {
                VideoDis video = new VideoDis();
                int mod = 0;
                video.Titulo = txtTitulo.Text;
                video.Duracion = Convert.ToInt32(txtDuracion.Text);
                video.Año = Convert.ToInt32(txtAño.Text);
                video.Reseña = txtReseña.Text;
                video.Genero = txtGenero.Text;
                video.Tipo = cbTipo.Text;
                if (video.Tipo == "Serie")
                {
                    video.Capitulos = Convert.ToInt32(txtCapitulos.Text);
                    video.Temporadas = Convert.ToInt32(txtTemporadas.Text);
                }
                mod = ObjvideoDis.ModificarVideo(video);
                if (mod == 1)
                {
                    lblMensaje.Text = "Base de Datos Actualizada";
                    txtTitulo.Text = "";
                    txtDuracion.Text = "";
                    txtAño.Text = "";
                    txtReseña.Text = "";
                    txtCapitulos.Text = "";
                    txtTemporadas.Text = "";

                }
                else
                {
                    lblMensaje.Text = "Video no Encontrado";
                }
            }
            
        }

        protected void Eliminar_Click(object sender, EventArgs e)
        {
            if (grillaVideos.Visible == true)
            {
                Button1.Visible = true;
            }
            VideoDis video = new VideoDis();
            int mod = 0;
            video.Titulo = txtTitulo.Text;
            mod = ObjvideoDis.EliminarVideo(video);
            if (mod == 1)
            {
                lblMensaje.Text = "Video Eliminado";
                txtTitulo.Text = "";
                txtDuracion.Text = "";
                txtAño.Text = "";
                txtReseña.Text = "";
                txtCapitulos.Text = "";
                txtTemporadas.Text = "";

            }
            else
            {
                lblMensaje.Text = "Video no Encontrado";
            }

        }
    }
}