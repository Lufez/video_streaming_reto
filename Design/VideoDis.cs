﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Design
{
    public class VideoDis
    {
        private string titulo;
        private int duracion;
        private int año;
        private string reseña;
        private string genero;
        private string tipo;
        private int capitulos;
        private int temporadas;

        public VideoDis()
        {

        }

        public VideoDis(String titulo, int duracion, int año, string reseña, string genero, 
            string tipo, int capitulos, int temporadas)
        {
            this.titulo = titulo;
            this.duracion = duracion;
            this.año = año;
            this.reseña = reseña;
            this.genero = genero;
            this.tipo = tipo;
            this.capitulos = capitulos;
            this.temporadas = temporadas;
        }

        public string Titulo
        {
            get { return titulo; }
            set { titulo = value; }
        }
        public int Duracion
        {
            get { return duracion; }
            set { duracion = value; }
        }
        public int Año
        {
            get { return año; }
            set { año = value; }
        }
        public string Reseña
        {
            get { return reseña; }
            set { reseña = value; }
        }
        public string Genero
        {
            get { return genero; }
            set { genero = value; }
        }
        public string Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }
        public int Capitulos
        {
            get { return capitulos; }
            set { capitulos = value; }
        }
        public int Temporadas
        {
            get { return temporadas; }
            set { temporadas = value; }
        }

    }
}
